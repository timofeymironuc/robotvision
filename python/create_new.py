from PIL import Image, ImageOps
from os import listdir

for i in listdir("../../../Загрузки/kegelDataSet"):
    im = Image.open(f"../../../Загрузки/kegelDataSet/{i}").resize((640, 640), Image.LANCZOS)
    ImageOps.mirror(im.rotate(30)).save(f"kegData/im{i}1.jpg")
    im.rotate(-30).save(f"kegData/im{i}2.jpg")
    ImageOps.mirror(im.rotate(-30)).save(f"kegData/im{i}3.jpg")
    im.rotate(30).save(f"kegData/im{i}4.jpg")
    ImageOps.mirror(im).save(f"kegData/im{i}5.jpg")
    im.save(f"kegData/im{i}6.jpg")

print(len(listdir("../../../Загрузки/kegelDataSet")))
