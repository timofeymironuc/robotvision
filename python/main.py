import torch
model = torch.hub.load('yolov5', 'custom', path='weights/keg2/best.pt', source='local')
# Image
img = ('detect.jpg')
# Inference
results = model(img)
# Results, change the flowing to: results.show()
print(results.pandas().xyxy[0].loc[1][:"class"])