import glob
import os

def get_filenames(folder):
    filenames = set()

    for path in glob.glob(os.path.join(folder, '*.jpg')):
        # Extract the filename
        filename = os.path.split(path)[-1]
        filenames.add(filename)

    return filenames


images = get_filenames("kegData/images")

import shutil


def split_dataset(image_names, train_size, val_size):
    for i, image_name in enumerate(image_names):
        # Label filename
        label_name = f"{image_name[:-4]}.txt"

        # Split into train, val, or test
        if i < train_size:
            split = 'train'
        elif i < train_size + val_size:
            split = 'val'
        else:
            split = 'test'

        # Source paths
        source_image_path = f'kegData/images/{image_name}'
        source_label_path = f'kegData/images/{label_name}'

        # Destination paths
        target_image_folder = f'kegData/final_dat/images/{split}'
        target_label_folder = f'kegData/final_dat/labels/{split}'

        # Copy files
        shutil.copy(source_image_path, target_image_folder)
        shutil.copy(source_label_path, target_label_folder)

# Cat data
split_dataset(images, train_size=400, val_size=53)
