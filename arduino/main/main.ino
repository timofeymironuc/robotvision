#include <Servo.h>
#include <NewPing.h>

const char echo = 12;
const char trig = 11;
const char servo = 2;
const char ENB = 5;
const char IN4 = 6;
const char IN3 = 7;
const char IN2 = 8;
const char IN1 = 9;
const char ENA =10;

float left = 1.0;
float right = 1.0;

NewPing sonar(trig, echo);
Servo myservo;

void setup() {
  // put your setup code here, to run once:
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENB, OUTPUT);
  Serial.begin(115200);
  myservo.attach(servo);
  forward();
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(sonar.ping_cm());
  myservo.write(sonar.ping_cm());
  delay(100);
  
}

void forward (){
  analogWrite(ENA, 255*left);
  analogWrite(ENB, 255*right);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
}
